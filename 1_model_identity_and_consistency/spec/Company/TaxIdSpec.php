<?php

namespace spec\Acme\Company;

use Acme\Company\Exceptions\InvalidTaxId;
use Acme\Company\TaxId;
use PhpSpec\ObjectBehavior;

class TaxIdSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->beConstructedWith('ABC-123-456');
        $this->shouldHaveType(TaxId::class);
    }

    function it_accepts_a_correctly_formatted_tax_id()
    {
        $this->beConstructedWith('HYS-726-106');
        $this->shouldHaveType(TaxId::class);
    }

    function it_rejects_any_form_of_invalid_tax_id()
    {
        $this->beConstructedWith('000-123-456');
        $this->shouldThrow(InvalidTaxId::class)->duringInstantiation();

        $this->beConstructedWith('ABCD-123-456');
        $this->shouldThrow(InvalidTaxId::class)->duringInstantiation();

        $this->beConstructedWith('');
        $this->shouldThrow(InvalidTaxId::class)->duringInstantiation();

        $this->beConstructedWith('ABC/123/456');
        $this->shouldThrow(InvalidTaxId::class)->duringInstantiation();

        $this->beConstructedWith('ABC123456');
        $this->shouldThrow(InvalidTaxId::class)->duringInstantiation();

        $this->beConstructedWith('ABC-123-4A6');
        $this->shouldThrow(InvalidTaxId::class)->duringInstantiation();

        $this->beConstructedWith('ABC-1SD-436');
        $this->shouldThrow(InvalidTaxId::class)->duringInstantiation();

        $this->beConstructedWith('FUCK-OFF-TWAT');
        $this->shouldThrow(InvalidTaxId::class)->duringInstantiation();
    }
}
