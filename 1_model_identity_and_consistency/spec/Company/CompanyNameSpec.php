<?php

namespace spec\Acme\Company;

use Acme\Company\CompanyName;
use Acme\Company\Exceptions\InvalidCompanyName;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CompanyNameSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->beConstructedWith('Acme Limited');
        $this->shouldHaveType(CompanyName::class);
    }

    function it_should_throw_an_exception_if_the_company_name_is_blank()
    {
        $this->beConstructedWith('');
        $this->shouldThrow(InvalidCompanyName::class)->duringInstantiation();
    }

    function it_should_throw_an_exception_if_the_company_name_is_empty()
    {
        $this->beConstructedWith('     ');
        $this->shouldThrow(InvalidCompanyName::class)->duringInstantiation();
    }
}
