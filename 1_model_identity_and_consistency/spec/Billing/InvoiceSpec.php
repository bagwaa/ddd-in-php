<?php

namespace spec\Acme\Billing;

use Ramsey\Uuid\Uuid;
use Acme\Company\TaxId;
use Acme\Billing\Invoice;
use PhpSpec\ObjectBehavior;
use Acme\Company\CompanyName;

class InvoiceSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->beConstructedWith(
            Uuid::uuid4(),
            new CompanyName('Acme Limited'),
            new TaxId('ABC-456-789')
        );

        $this->shouldHaveType(Invoice::class);
    }
}
