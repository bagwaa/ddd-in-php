<?php

namespace Acme\Billing;

use Acme\Company\CompanyName;
use Acme\Company\TaxId;
use Ramsey\Uuid\Uuid;

class Invoice
{
    /**
     * @var Uuid
     */
    private $uuid;

    /**
     * @var CompanyName
     */
    private $companyName;

    /**
     * @var TaxId
     */
    private $taxId;

    public function __construct(Uuid $uuid, CompanyName $companyName, TaxId $taxId)
    {
        $this->uuid = $uuid;
        $this->companyName = $companyName;
        $this->taxId = $taxId;
    }
}
