<?php

namespace Acme\Company;

use Acme\Company\Exceptions\InvalidTaxId;

class TaxId
{
    /**
     * @var string
     */
    private $taxId;

    public function __construct(string $taxId)
    {
        if (! $this->validTaxId($taxId)) {
            throw new InvalidTaxId;
        }

        $this->taxId = $taxId;
    }

    /**
     * @param string $taxId
     * @return bool
     */
    private function validTaxId(string $taxId)
    {
        return (bool) preg_match('/^[a-zA-Z]{3}-\d{3}-\d{3}$/', $taxId);
    }
}
