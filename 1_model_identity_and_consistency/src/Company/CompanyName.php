<?php

namespace Acme\Company;

use Acme\Company\Exceptions\InvalidCompanyName;

final class CompanyName
{
    /**
     * @var string
     */
    private $name;

    public function __construct(string $name)
    {
        $this->checkNameIsValid(
            $this->sanitize($name)
        );

        $this->name = $name;
    }

    /**
     * @param string $name
     * @return string
     */
    private function sanitize(string $name): string
    {
        return trim($name);
    }

    /**
     * @param string $name
     * @throws InvalidCompanyName
     */
    private function checkNameIsValid(string $name): void
    {
        if (strlen($name) < 1) {
            throw new InvalidCompanyName;
        }
    }
}
