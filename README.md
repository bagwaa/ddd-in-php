# DDD By Example

Each folder contains an example of a particular DDD concept, in each folder simply run :-

* composer install

And then running the tests is as simple as :-

* vendor/bin/phpspec run

These example may not necessarily do anything, but are intended to be used as an example of how to handle these concepts in POPO (Plain Old PHP Objects).